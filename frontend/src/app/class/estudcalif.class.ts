export class Estudcalif {
	public id_calftema: number;
	public id_estudiante: number;
	public fecha: Date;

	constructor(
		id_calftema: number,
		id_estudiante: number,
		fecha: Date){

      this.id_calftema = id_calftema;
      this.id_estudiante = id_estudiante;
	    this.fecha = fecha;

	}

}
